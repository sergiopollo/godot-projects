extends Area2D

signal levelComplete

enum states{
	open,closed
}

var currentState = states.closed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func openDoor():
	$AnimatedSprite2D.play("open")
	pass

func _on_body_entered(body):
	print("flag-coll", body);
	
	if body.is_in_group("player") && currentState == states.open:
		levelComplete.emit()
	
	pass # Replace with function body.


func _on_animated_sprite_2d_animation_finished():
	currentState = states.open
	pass # Replace with function body.
