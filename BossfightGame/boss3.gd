extends RigidBody2D

var hp = 20
var atk = 1
var score = 100

var changeDir = -1

var pos1 = get_global_position()
var pos2 = get_global_position() + Vector2(30, 0)
var pos3 = get_global_position() + Vector2(-30, 0)

enum states {idle, attacking}
var currentState = states.idle

var attacks = [1,2]
var currentAtk = 1
var rng = RandomNumberGenerator.new()

var timerAtk= 0
var timerAtkMax = 150

var timerPos = 0
var timerPosMax = 1000
var changePos = true
	

@export var flyAtkPositions: Array[Marker2D]
@export var sideAtkPositions: Array[Marker2D]

@export var projectile: PackedScene

# Called when the node enters the scene tree for the first time.
func _ready():
	
	#linear_velocity.x = 8;
	
	changeHUD();
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#position.x +=1;
	#linear_velocity.x = 5;
	
	#linear_velocity = Vector2(80*changeDir,linear_velocity.y);
	
	#apply_force(Vector2(100,0))
	
	#linear_velocity.y+=500*delta
	#print("pos1 ", to_global(pos1))
	
#	changeHUD();
	
	pass

func _physics_process(delta):
	
	var space_state = get_world_2d().direct_space_state
	# use global coordinates, not local to node
	
	var query = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos2), 2)
	var query2 = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos2), 1)
	
	if(changeDir==-1):
		#query = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos3))
		query = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos3++Vector2(0, 5)), 2)
		query2 = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos3++Vector2(0, 5)), 1)
	else:
		#query = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos2))
		query = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos2++Vector2(0, 5)), 2)
		query2 = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos2++Vector2(0, 5)), 1)
		
	query.exclude = [self]
	query2.exclude = [self]
	var result = space_state.intersect_ray(query)
	var result2 = space_state.intersect_ray(query2)
	
	#DrawLine2D.DrawLine(pos1, pos2, Color(0, 0, 1), 1.5)
	
	if result || result2:
		#print("Hit at point: ", result.position)
		changeDir = changeDir*-1
		#print(changeDir)
		if(changeDir == 1):
			$AnimatedSprite2D.flip_h = true
		else:
			$AnimatedSprite2D.flip_h = false
	
	
	'''
	if(is_on_wall()):
		linear_velocity = Vector2(0,linear_velocity.y);
	else:
		linear_velocity = Vector2(80,linear_velocity.y);
	'''
	
	
	
	
	
	
	
	if(changePos):
		changePos = false
		
		var random = rng.randi_range(1, attacks.size())
		
		if(random == 1):
			currentAtk = 1
			
			var random2 = rng.randi_range(0, 1)
			self.global_position = flyAtkPositions[random2].get_global_transform().get_origin()
			
		else:
			currentAtk = 2
			var random2 = rng.randi_range(0, 1)
			self.global_position = sideAtkPositions[random2].get_global_transform().get_origin()
			
			if(random2 == 0):
				$AnimatedSprite2D.flip_h = true
			else:
				$AnimatedSprite2D.flip_h = false
	else:
		if(timerPos < timerPosMax):
			timerPos+=1
		else:
			timerPos = 0
			changePos=true
	
	if(currentAtk == 1):
		move_and_collide(Vector2(1*changeDir,0))
	
	if(currentState != states.attacking):
		currentState = states.attacking
		$AnimatedSprite2D.play("attack")
#		var bullet = projectile.instantiate()
#		bullet.position = self.position
#		if(currentAtk == 1):
#			bullet.isVertical = true
#		if($Sprite2D.flip_h == false):
#			bullet.changeDir = -1
#		get_parent().add_child(bullet)
		
	else:
		if(timerAtk < timerAtkMax):
			timerAtk+=1
		else:
			timerAtk = 0
			currentState = states.idle
	
	pass


#func _draw():
#
#	#if(changeDir == -1):
#		draw_line(pos1, pos3, Color(1,0,0), 1)
#	#else:
#		draw_line(pos1, pos2, Color(1,0,0), 1)
#pass

func changeHUD():
	
	#var hudNode = get_parent().find_child("HUD")
	#var hudNode = get_parent().get_parent().find_child("HUD")
	var hudNode = get_tree().get_root().get_node("menu").find_child("HUD")
	#if(hudNode == null):
	#	hudNode = get_parent().get_parent().find_child("HUD")
	print("HUD! ",hudNode)
	#var hudNode = $HUD
	if(hudNode != null):
		hudNode.changeBossHP(self.hp)
	pass

func drawRaycast():
	pass




func _on_body_entered(body):
	
	#print("enemigohit")
	if body.is_in_group("bullet"):
		getHit(1)
	
	
	pass # Replace with function body.
	
func getHit(dmg):
	
	changeHUD();
	
	var tween: Tween = create_tween()
	tween.tween_property($AnimatedSprite2D, "modulate:v", 1, 0.1).from(15)
	
	if(hp-dmg <= 0):
		hp=0
	else:	
		hp-=dmg
	print("enemy HP ", hp)
	
	if(hp == 0):
		print("enemy DIE")
		die()
	pass

func die():
	hide() # Player disappears after being hit.
	#hit.emit()
	print("meborroENEMY");
	
	var playerNode = get_parent().find_child("Player")
	if(playerNode != null):
		playerNode.addScore(self.score)
		
	if(get_parent().has_method("openDoor")):
		get_parent().openDoor()
	else:
		var flagNode = get_parent().find_child("flag")
		flagNode.openDoor()
	
	# Must be deferred as we can't change physics properties on a physics callback.
	$CollisionShape2D.set_deferred("disabled", true)
	queue_free()


func _on_animated_sprite_2d_animation_finished():
	if(currentAtk == 2):
		$AnimatedSprite2D.play("idle")
	else:
		$AnimatedSprite2D.play("walk")
		
	var bullet = projectile.instantiate()
	bullet.position = self.position
	if(currentAtk == 1):
		bullet.isVertical = true
	if($AnimatedSprite2D.flip_h == true):
		bullet.changeDir = 1
	get_parent().add_child(bullet)
	pass # Replace with function body.
