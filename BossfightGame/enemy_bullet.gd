extends RigidBody2D

var hp = 3
var atk = 1
var score = 100

@export var changeDir = -1

@export var isVertical = false

var pos1 = get_global_position()
var pos2 = get_global_position() + Vector2(8, 0)
var pos3 = get_global_position() + Vector2(-8, 0)
var pos4 = get_global_position() + Vector2(0, 8)

# Called when the node enters the scene tree for the first time.
func _ready():
	
	#linear_velocity.x = 8;
	$AnimatedSprite2D.play("default")
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#position.x +=1;
	#linear_velocity.x = 5;
	
	#linear_velocity = Vector2(80*changeDir,linear_velocity.y);
	
	#apply_force(Vector2(100,0))
	
	#linear_velocity.y+=500*delta
	#print("pos1 ", to_global(pos1))
	
	
	pass

func _physics_process(delta):
	
	var space_state = get_world_2d().direct_space_state
	# use global coordinates, not local to node
	
	var query = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos2), 2)
	var query2 = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos2), 1)
	
	if(isVertical):
		query = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos4++Vector2(0, 5)), 2)
		query2 = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos4++Vector2(0, 5)), 1)
	else:
		if(changeDir==-1):
			#query = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos3))
			query = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos3++Vector2(0, 5)), 2)
			query2 = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos3++Vector2(0, 5)), 1)
		else:
			#query = PhysicsRayQueryParameters2D.create(to_global(pos1), to_global(pos2))
			query = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos2++Vector2(0, 5)), 2)
			query2 = PhysicsRayQueryParameters2D.create(to_global(pos1+Vector2(0, 5)), to_global(pos2++Vector2(0, 5)), 1)
		
	
	query.exclude = [self]
	query2.exclude = [self]
	var result = space_state.intersect_ray(query)
	var result2 = space_state.intersect_ray(query2)
	
	#DrawLine2D.DrawLine(pos1, pos2, Color(0, 0, 1), 1.5)
	
	if result || result2:
		#print("Hit at point: ", result.position)
		#changeDir = changeDir*-1
		#print(changeDir)
		getHit(99999)
	
	'''
	if(is_on_wall()):
		linear_velocity = Vector2(0,linear_velocity.y);
	else:
		linear_velocity = Vector2(80,linear_velocity.y);
	'''
	if(hp > 0):
		if(isVertical):
			move_and_collide(Vector2(0,2))
		else: 
			move_and_collide(Vector2(1.5*changeDir,0))
	pass


#func _draw():
#
#	#if(changeDir == -1):
##		draw_line(pos1, pos3, Color(1,0,0), 1)
##	#else:
##		draw_line(pos1, pos2, Color(1,0,0), 1)
##
##		draw_line(pos1, pos4, Color(1,0,0), 1)
#pass



func drawRaycast():
	pass




func _on_body_entered(body):
	
	#print("enemigohit")
	if body.is_in_group("bullet"):
		getHit(1)
	else:
		die()
	
	pass # Replace with function body.
	
func getHit(dmg):
	
	if(hp-dmg <= 0):
		hp=0
	else:	
		hp-=dmg
	print("enemy HP ", hp)
	
	if(hp == 0):
		print("enemy DIE")
		boom()
	pass

func boom():
	$AnimatedSprite2D.play("boom")

func die():
	hide() # Player disappears after being hit.
	#hit.emit()
	print("meborroENEMY");
	
	
	# Must be deferred as we can't change physics properties on a physics callback.
	$CollisionShape2D.set_deferred("disabled", true)


func _on_animated_sprite_2d_animation_finished():
	if(hp <= 0):
		die()
	pass # Replace with function body.
