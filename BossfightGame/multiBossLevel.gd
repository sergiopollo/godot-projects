extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var hud = get_tree().get_root().get_node("menu").find_child("HUD")
	
	var b1 = find_child("boss2")
	var b2 = find_child("boss3")
	
	var hp1 = 0
	var hp2 = 0
	
	if(b1 != null):
		hp1 = b1.hp
	if(b2 != null):
		hp2 = b2.hp

	hud.changeBossHP(str(hp1, " + ", hp2))


func openDoor():
	var b1 = find_child("boss2")
	
	var b2 = find_child("boss2")
	
	if(b1 == null && b1 == null):
		var flag = find_child("flag")
		flag.openDoor()
	
	pass
