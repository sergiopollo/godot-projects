extends CharacterBody2D


@export var normalSprite: Texture
@export var hurtSprite: Texture

@export var bullet_scene: PackedScene
var unlockedDoubleJump=true
var hasDoubleJump=false

var shootCooldown = 0
var canShoot = true
var shootCooldownMax = 50
var shootCooldownMaxDefault = 50

var powerupDuration = 0

var invincibleCooldown = 0
var invincibleTime = 100

var currentState = states.normal

enum states {
	
	normal, invincible, dead
}

var hp = 3
var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	changeHUD()
	pass # Replace with function body.


func _process(delta):
	print("shootCooldownMax ", shootCooldownMax)
	if(powerupDuration > 0):
		print("powerupDurationaa ", powerupDuration)
		powerupDuration -=1
	else:
		shootCooldownMax = shootCooldownMaxDefault
	
	#el clasico, mitico y horrible cooldown
	if(shootCooldown < shootCooldownMax):
		canShoot = false
		shootCooldown +=1	
	else:
		canShoot=true
		
	if(currentState == states.invincible):
		invincibleCooldown +=1
		
		if(invincibleCooldown % 5 == 0):
			if($Sprite2D.texture == hurtSprite):
				$Sprite2D.texture = normalSprite
			else:
				$Sprite2D.texture = hurtSprite
		
		if(invincibleCooldown	>= invincibleTime):
			invincibleCooldown=0
			changeState(states.normal)

func changeState(state):
	currentState = state
	match currentState:
		states.normal:
			$Sprite2D.texture = normalSprite
			self.set_collision_mask_value(2, true)
		states.invincible:
			$Sprite2D.texture = hurtSprite
			self.set_collision_mask_value(2, false)
	pass

func jump():
	#print(self.global_position)
	if(is_on_floor() || (hasDoubleJump && unlockedDoubleJump && not is_on_wall())):
		if(not is_on_floor()):
			hasDoubleJump=false
			velocity.y=0;
		velocity.y -= 200	
	elif(is_on_wall()):
		
		var wall_jump_pushback=400
		var jump_power=-200
		
		
		velocity = Vector2(get_wall_normal().x * wall_jump_pushback, jump_power)
		
#		print(get_wall_normal())
#
#		var jumpSpeedX=1000
#		var jumpSpeedY=1000
#
#		velocity.y=0;
#		if(get_wall_normal().x < 0):
#			velocity.x -= jumpSpeedX
#		else:
#			velocity.x += jumpSpeedX
#
#		if(velocity.y >= jumpSpeedY):
#			velocity.y -= jumpSpeedY

		

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
	#player respawn
	if(position.y > 500):
		position = Vector2(0,-20)
		getHit(1)
	
	#print(hasDoubleJump)
	
	if not is_on_floor():
		if(is_on_wall_only() && velocity.y > 0):
		
			velocity.y = 50
		else:
			velocity.y+=500*delta
	elif(unlockedDoubleJump):
		hasDoubleJump=true
			
	if(not is_on_wall_only()):
		velocity.x=0
	
	if(currentState != states.dead):
		if Input.is_key_pressed(KEY_D):
			velocity.x += 100
			$Sprite2D.flip_h = true
		if Input.is_key_pressed(KEY_A):
			velocity.x -= 100
			$Sprite2D.flip_h = false
			
		if Input.is_mouse_button_pressed(MOUSE_BUTTON_LEFT) && canShoot:
			shootCooldown=0
			var bullet = bullet_scene.instantiate()
			bullet.position = self.position
			if($Sprite2D.flip_h == false):
				bullet.direction = -1
			get_parent().add_child(bullet)
			pass
		
		if Input.is_action_just_pressed("jump"):
		#if Input.is_key_pressed(KEY_SPACE):
			jump()
		
		
	#var collision = move_and_collide(velocity * delta)
	#print(collision)
	
	for i in get_slide_collision_count():
		var collision = get_slide_collision(i)
		#print("Collided with: ", collision.get_collider().name)
		
	move_and_slide()
	
	for i in get_slide_collision_count():
		var collision = get_slide_collision(i)
		if(collision.get_collider().is_in_group("enemy")):
			print("I collided with ", collision.get_collider().name)
			print(collision.get_collider().atk)
			getHit(collision.get_collider().atk)

func getHit(dmg):
	
	if(currentState != states.invincible && currentState != states.dead):
		if(hp-dmg <= 0):
			hp=0
		else:	
			hp-=dmg
		print("HP ", hp)
		
		if(hp == 0):
			print("DIE")
			changeState(states.dead)	
		else:
			changeState(states.invincible)
		
		changeHUD()
	
	pass

func addScore(newScore):
	
	self.score +=newScore
	changeHUD() 
	
	pass

	
func changeHUD():
	
	#var hudNode = get_parent().find_child("HUD")
	#var hudNode = get_parent().get_parent().find_child("HUD")
	var hudNode = get_tree().get_root().get_node("menu").find_child("HUD")
	#if(hudNode == null):
	#	hudNode = get_parent().get_parent().find_child("HUD")
	print("HUD! ",hudNode)
	#var hudNode = $HUD
	if(hudNode != null):
		hudNode.changeHP(self.hp)
		hudNode.changeScore(self.score)
		if(currentState == states.dead):
			hudNode.show_buttons()
	
	pass
