extends Area2D

var score = 10
var powerupTime = 500
var newShootCooldown = 5

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite2D.play("default")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_body_entered(body):
	print("collected!", body);
	
	$AudioStreamPlayer2D.play()
	var playerNode = get_parent().find_child("Player")
	if(playerNode != null):
		playerNode.powerupDuration = self.powerupTime
		playerNode.shootCooldownMax = self.newShootCooldown
	
	hide()
	print("meborro");
	# Must be deferred as we can't change physics properties on a physics callback.
	$CollisionShape2D.set_deferred("disabled", true)
	
	pass # Replace with function body.
