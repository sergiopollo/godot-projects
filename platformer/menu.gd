extends Node2D

signal showMenu

class level:
	var levelName
	var score
	var isUnlocked
	var sceneName
	var flagCoords
	
	func _init(lvn, sc, isUn, sn, fCoords):
		levelName=lvn
		score=sc
		isUnlocked=isUn
		sceneName = sn
		flagCoords = fCoords

@export var levelScenes:Array[PackedScene]
var levels: Array[level]
var index: int = 0

var currentLevelScene

var flagItem

# Called when the node enters the scene tree for the first time.
func _ready():
	
	levels.append(level.new("nivel 1", 0, true, levelScenes[0], Vector2(50,-50)))
	levels.append(level.new("nivel 2", 0, false, levelScenes[1], Vector2(10,10)))
	
	loadMenu();
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func loadMenu():
	changeLevel(levels[0])

func changeLevel(newLevel: level):
	currentLevelScene = levelScenes[index]
	
	$MenuCanvas/levelName.text = newLevel.levelName
	if(newLevel.isUnlocked):
		$MenuCanvas/levelImageLocked.visible=false
	else:
		$MenuCanvas/levelImageLocked.visible=true

func _on_button_start_pressed():
	if(levels[index].isUnlocked):
		loadLevelScene()
		$MenuCanvas.visible = false
		$HUD.visible = true
	pass # Replace with function body.


func _on_button_next_pressed():
	if(levels.size()-1 <= index):
		index = 0
	else:
		index +=1
	
	changeLevel(levels[index])
	pass # Replace with function body.


func _on_button_exit_pressed():
	_notification(NOTIFICATION_WM_CLOSE_REQUEST)
	pass # Replace with function body.


func _showMenu():
	pass

func _notification(what):
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		get_tree().quit() # default behavior

func loadLevelScene():
	#$flag.global_position = levels[index].flagCoords
	currentLevelScene = levelScenes[index].instantiate()
	get_parent().add_child(currentLevelScene)
	var flag = currentLevelScene.find_child("flag")
	flag.levelComplete.connect(_on_flag_level_complete)
	pass

func deleteLevelScene():
	for child in currentLevelScene.get_children():
		child.queue_free()
	currentLevelScene.queue_free()
	pass

func _on_hud_go_back_to_menu():
	print(currentLevelScene)
	
	deleteLevelScene()
	
	$MenuCanvas.visible = true
	$HUD.visible = false
	pass # Replace with function body.


func _on_hud_restart_level():
	deleteLevelScene()
	loadLevelScene()
	pass # Replace with function body.
	


func _on_flag_level_complete():
	print("OH MAH GAAAAHD HAS GANAO MI PANA");
	_on_hud_go_back_to_menu()
	if(index+1 < levels.size()):
		levels[index+1].isUnlocked = true;
	loadMenu()
	pass # Replace with function body.
