extends CanvasLayer


signal goBackToMenu
signal restartLevel

# Called when the node enters the scene tree for the first time.
func _ready():
	show_buttons()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func show_buttons():
	if($RestartButton.visible):
		$MenuButton.hide()
		$RestartButton.hide()
	else:
		$MenuButton.show()
		$RestartButton.show()

func changeScore(newScore):
	$ScoreLabel.text = "Score: "+str(newScore)

func changeHP(newHP):
	$HPLabel.text = "HP: "+str(newHP)


func _on_restart_button_pressed():
	#get_parent().get_tree().reload_current_scene()
	restartLevel.emit()
	show_buttons()
	pass # Replace with function body.


func _on_menu_button_pressed():
	show_buttons()
	goBackToMenu.emit()
	pass # Replace with function body.

